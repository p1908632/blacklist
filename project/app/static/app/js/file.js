// Vérification de la présence des éléments avant d'effectuer des opérations
const targetList = document.querySelector('.target-list');
const cibleForm = document.getElementById('cible-form');

if (targetList && cibleForm) {
    // Ajout d'un écouteur d'événement pour le bouton "Ajouter une Cible"
    targetList.addEventListener('click', (event) => {
        if (event.target.classList.contains('show-form-btn')) {
            toggleCibleForm();
        }
    });

    // Écouteur d'événements pour le survol de la ligne
    targetList.addEventListener('mouseover', (event) => {
        if (event.target.classList.contains('target-container')) {
            showButton(event.target);
        }
    });

    // Écouteur d'événements pour quitter la ligne
    targetList.addEventListener('mouseout', (event) => {
        if (event.target.classList.contains('target-container')) {
            hideButton(event.target);
        }
    });
}

// Fonction pour afficher ou cacher le formulaire
function toggleCibleForm() {
    cibleForm.classList.toggle('visible-form');
}

// Fonction pour afficher le bouton "Modifier" et le contenu en déroulement au survol de la ligne
function showButton(row) {
    const editButton = row.querySelector('.edit-button');
    const hiddenContent = row.querySelector('.hidden-content');

    if (editButton && hiddenContent) {
        editButton.style.display = 'inline-block';
        hiddenContent.style.display = 'block';
    }
}

// Fonction pour cacher le bouton "Modifier" et le contenu en déroulement lorsque la souris quitte la ligne
function hideButton(row) {
    const editButton = row.querySelector('.edit-button');
    const hiddenContent = row.querySelector('.hidden-content');

    if (editButton && hiddenContent) {
        editButton.style.display = 'none';
        hiddenContent.style.display = 'none';
    }
}

// Fonction pour rediriger vers la page de modification avec l'ID de la cible
function editTarget(targetId) {
    window.location.href = `/edit_target/${targetId}/`;
}
