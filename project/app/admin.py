from django.contrib import admin

from .models import Categorie, Cible

admin.site.register(Categorie)
admin.site.register(Cible)
