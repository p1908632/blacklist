from django.db import models

class Categorie(models.Model):
    nom_categorie = models.CharField(max_length=200)
    def __str__(self):
        return self.nom_categorie


class Cible(models.Model):
    classement = models.IntegerField(unique=True)
    nom = models.CharField(max_length=200)
    prenom = models.CharField(max_length=200)
    categorie = models.ForeignKey(Categorie, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.classement) + ") " + self.nom.upper() + " " + self.prenom.title() + " : " + str(self.categorie)