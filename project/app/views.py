from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404
from django.template import loader
from .forms import CibleForm
from .models import Cible, Categorie


def index(request):
    worst_targets = Cible.objects.order_by("classement")[:5]
    context = {"worst_targets" : worst_targets}
    return render(request, "app/index.html", context)

# Leave the rest of the views (detail, results, vote) unchanged

def detail(request, categorie_id):
    categorie = get_object_or_404(Categorie, id=categorie_id)
    return render(request, "app/detail.html", {"categorie": categorie})


def results(request, cible_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % cible_id)


def vote(request, cible_id):
    return HttpResponse("You're voting on question %s." % cible_id)

def get_list(request):
    targets = Cible.objects.all()
    context = {"targets" : targets}
    return render(request, "app/body.html", context)


def add_cible_view(request):

    if request.method == 'POST':
        form = CibleForm(request.POST)
        if form.is_valid():
            # Calculate the new classement
            total_cibles = Cible.objects.count()
            new_classement = total_cibles + 1

            # Create and save the new cible
            cible = form.save(commit=False)
            cible.classement = new_classement
            cible.save()
            return redirect('get_list')  # Redirige vers la page principale après l'ajout
    else:
        form = CibleForm()

    return render(request, 'app/add_cible.html', {'form': form})
