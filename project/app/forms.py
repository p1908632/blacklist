from django import forms
from .models import Cible

class CibleForm(forms.ModelForm):
    class Meta:
        model = Cible
        fields = ['nom', 'prenom', 'categorie']