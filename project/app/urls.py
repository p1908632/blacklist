from django.urls import path

from . import views

urlpatterns = [
    # ex: /app/5/
    path("<int:categorie_id>/", views.detail, name="detail"),
    # ex: /app/5/results/
    path("<int:cible_id>/results/", views.results, name="results"),
    # ex: /app/5/vote/
    path("<int:cible_id>/vote/", views.vote, name="vote"),
    path("", views.get_list, name="get_list"),
    path("cible/add/", views.add_cible_view, name='cible_add'),
]